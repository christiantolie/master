package id.co.ekost.core.ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class EkostCoreApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(EkostCoreApplication.class);
	}

	public static void main(String[] args) {
//		new EkostCoreApplication().configure(new SpringApplicationBuilder(EkostCoreApplication.class)).run(args);
		SpringApplication.run(EkostCoreApplication.class, args);
	}

}
