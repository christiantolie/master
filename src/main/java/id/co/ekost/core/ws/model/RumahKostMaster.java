package id.co.ekost.core.ws.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RumahKostMaster {

    private Integer id;
    private Integer userId;
    private String kostName;
    private String kostDesc;
    private String kostLocation;
    private String latitude;
    private String longitude;
    private String kostAddress;
    private String picName;
    private String picPhoneNumber;
    private Timestamp createdDate;
    private Timestamp updatedDate;


}
