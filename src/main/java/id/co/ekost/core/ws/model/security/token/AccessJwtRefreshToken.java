//package id.co.ekost.core.ws.model.security.token;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import io.jsonwebtoken.Claims;
//
//public class AccessJwtRefreshToken implements JwtToken {
//
//    private final String rawToken;
//    private final String refreshToken;
//
//    @JsonIgnore
//    private Claims claims;
//
//    protected AccessJwtRefreshToken(final String token, final String refreshToken, Claims claims) {
//        this.rawToken = token;
//        this.claims = claims;
//        this.refreshToken = refreshToken;
//    }
//
//    public String getToken() {
//        return this.rawToken;
//    }
//
//    public String getRefreshToken() {
//        return this.refreshToken;
//    }
//
//    public Claims getClaims() {
//        return this.claims;
//    }
//
//}
