package id.co.ekost.core.ws.controller.midtrans;

import id.co.ekost.core.ws.model.RumahKostMaster;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedHashMap;
import java.util.List;

@Component
@Path("transaction")
public class SnapController {

    @GET
    @Path("/snap")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response snap (@Context HttpHeaders headers ) {
//        List<RumahKostMaster> rumahKostMasterLists = mapper.findAll(offset, limit);

        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        res.put("status", "OK");
        res.put("message", "Success");
//        res.put("total", rumahKostMasterLists.size());
//        res.put("data", rumahKostMasterLists);
        return Response.ok().entity(res).build();
    }


}
