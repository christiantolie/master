package id.co.ekost.core.ws.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserMaster {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String fullName;
    private String userName;
    private String email;
    private String passwordHash;
    private String avatarFileName;
    private String role;
    private String token;
    private Timestamp createdDate;
    private Timestamp updatedDate;
}
