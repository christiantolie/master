package id.co.ekost.core.ws.controller;

import com.google.gson.Gson;
import id.co.ekost.core.ws.mapper.UserMapper;
import id.co.ekost.core.ws.model.AuthRequest;
import id.co.ekost.core.ws.model.UserMaster;
import id.co.ekost.core.ws.service.CustomUserDetailsService;
import id.co.ekost.core.ws.util.JwtUtil;
import org.apache.catalina.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.sql.Timestamp;
import java.util.LinkedHashMap;

@Component
@Path("auth")
public class AuthEndpoint {

    @Autowired
    UserMapper userMapper;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    static Logger logger = LoggerFactory.getLogger(AuthEndpoint.class);


    @POST
    @Path("/signin")
    public String generateToken(@RequestBody AuthRequest authRequest) throws Exception {
           try {
                System.out.println("authenticate ");
                Authentication authenticate = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(authRequest.getUserName(), authRequest.getPasswordHash()));
//               newData.setPasswordHash(new BCryptPasswordEncoder().encode(newData.getPasswordHash()));
                System.out.println("auth " + new Gson().toJson(authenticate));
                return jwtUtil.generateToken(authRequest.getUserName());
            } catch (Exception ex) {
                throw new Exception("inavalid username/password");
            }

    }

    @POST
    @Path("signup")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response add(@Context HttpServletRequest request, UserMaster newData) {

        newData.setPasswordHash(new BCryptPasswordEncoder().encode(newData.getPasswordHash()));

        newData.setCreatedDate(new Timestamp(System.currentTimeMillis()));

        userMapper.saveUser(newData);

        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        res.put("Status", "ok");
        res.put("message", "success");
        res.put("data", newData);

        return Response.ok().entity(res).build();    
    }
}
