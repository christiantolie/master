package id.co.ekost.core.ws.mapper;

import id.co.ekost.core.ws.model.RumahKostMaster;
import id.co.ekost.core.ws.model.UserMaster;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository

public interface UserMapper {

    UserMaster findByUserName(String username);
    int saveUser(UserMaster userMaster);
//    int UserMaster createUser(UserMaster userMaster);
}
