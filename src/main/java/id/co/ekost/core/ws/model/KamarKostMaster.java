package id.co.ekost.core.ws.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KamarKostMaster {
    @NonNull
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer kostId;
    private String kamarName;
    private String kamarDesc;
    private Integer kamarSummary;
    private Integer luas;
    private Integer lebar;
    private String kamarGender;
    private String kamarPrice;
    private Timestamp createdDate;
    private Timestamp updatedDate;
}
