package id.co.ekost.core.ws.config;

//import id.co.ekost.core.ws.controller.KostFinderEndpoint;
import id.co.ekost.core.ws.controller.KostFinderEndpoint;
import id.co.ekost.core.ws.controller.KostOwnerEndpoint;
import id.co.ekost.core.ws.controller.AuthEndpoint;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JerseyConfig extends ResourceConfig {
    @Autowired
    public JerseyConfig() {
        register(KostOwnerEndpoint.class);
        register(KostFinderEndpoint.class);
        register(AuthEndpoint.class);
    }
}
