package id.co.ekost.core.ws.controller;


import id.co.ekost.core.ws.mapper.RumahKostMapper;

import id.co.ekost.core.ws.model.RumahKostMaster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedHashMap;
import java.util.List;

@Component
@Path("kost/finder")
public class KostFinderEndpoint {


    static Logger logger = LoggerFactory.getLogger(KostFinderEndpoint.class);

    @Autowired
    private RumahKostMapper mapper;

    @GET
    @Path("/getlistkost")
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetAllListKost (@Context HttpHeaders headers, @DefaultValue("0") @QueryParam("offset") Integer offset, @QueryParam("limit") Integer limit) {
        List<RumahKostMaster> rumahKostMasterLists = mapper.findAll(offset, limit);

        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        res.put("status", "OK");
        res.put("message", "Success");
        res.put("total", rumahKostMasterLists.size());
        res.put("data", rumahKostMasterLists);
        return Response.ok().entity(res).build();
    }

    @GET
    @Path("/getlistkostbycity")
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetAllListByCity (@Context HttpHeaders headers,
                                      @QueryParam("kostLocation") String kostLocation,
                                      @DefaultValue("0") @QueryParam("offset") Integer offset,
                                      @QueryParam("limit") Integer limit) {
        List<RumahKostMaster> rumahKostMasterLists = mapper.findAllByCity(kostLocation, offset, limit);

        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        res.put("status", "OK");
        res.put("message", "Success");
        res.put("total", rumahKostMasterLists.size());
        res.put("data", rumahKostMasterLists);
        return Response.ok().entity(res).build();
    }

}
