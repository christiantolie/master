package id.co.ekost.core.ws.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionMaster {

        private Integer id;
        private Integer kamarId;
        private Integer userId;
        private String price;
        private String status;
        private String code;
        private Timestamp createdDate;
        private Timestamp updatedDate;
        private String paymentURL;

}
