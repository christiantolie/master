package id.co.ekost.core.ws.mapper;

import id.co.ekost.core.ws.model.TransactionMaster;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface TransactionMapper {
    int transactionKamar (TransactionMaster transactionMaster);
}
