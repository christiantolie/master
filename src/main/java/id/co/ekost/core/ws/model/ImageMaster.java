package id.co.ekost.core.ws.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImageMaster {
    private String kostId;
    private String fileName;
    private boolean isPrimary;
    private Date createdDate;
    private Date updatedDate;

}
