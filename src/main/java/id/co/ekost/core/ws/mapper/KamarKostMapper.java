package id.co.ekost.core.ws.mapper;

import id.co.ekost.core.ws.model.KamarKostMaster;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface KamarKostMapper {
    int createKamar(KamarKostMaster kamarKostMaster);
}
