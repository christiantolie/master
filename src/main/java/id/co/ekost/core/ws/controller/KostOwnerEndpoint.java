package id.co.ekost.core.ws.controller;

import id.co.ekost.core.ws.mapper.KamarKostMapper;
import id.co.ekost.core.ws.mapper.RumahKostMapper;
import id.co.ekost.core.ws.model.KamarKostMaster;
import id.co.ekost.core.ws.model.RumahKostMaster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.List;

@Component
@Path("kost/owner")
public class KostOwnerEndpoint {

    static Logger logger = LoggerFactory.getLogger(KostOwnerEndpoint.class);


    @Autowired
    private RumahKostMapper mapper;

    @Autowired
    private KamarKostMapper kamarKostMapper;

    @POST
    @Path("/createkost")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createkost(@Context HttpHeaders headers, RumahKostMaster param) {
        param.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        mapper.createRumah(param);
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        res.put("status", "OK");
        res.put("message", "Success");
        res.put("data", param);
        return Response.ok().entity(res).build();
    }

    @POST
    @Path("/createkamar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createkamar(@Context HttpHeaders headers, KamarKostMaster param) {
        param.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        kamarKostMapper.createKamar(param);
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        res.put("status", "OK");
        res.put("message", "Success");
        res.put("data", param);
        return Response.ok().entity(res).build();
    }


    @GET
    @Path("/getkostlist")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getKamar(@Context HttpHeaders headers,
                             @DefaultValue("0") @QueryParam("offset") Integer offset,
                             @QueryParam("limit") Integer limit) {
        List<RumahKostMaster> list = mapper.findAllRumahDanKamar(offset, limit);
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        res.put("status", "OK");
        res.put("message", "Success");
        res.put("data", list);
        return Response.ok().entity(res).build();
    }


}
