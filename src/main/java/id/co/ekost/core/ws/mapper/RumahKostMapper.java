package id.co.ekost.core.ws.mapper;

import id.co.ekost.core.ws.model.RumahKostMaster;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import javax.ws.rs.QueryParam;
import java.sql.Timestamp;
import java.util.List;

@Mapper
@Repository
public interface RumahKostMapper {
    int createRumah(RumahKostMaster rumahKostMaster);
    List<RumahKostMaster> findAll(@Param("offset") Integer offset, @Param("limit") Integer limit);
    List<RumahKostMaster> findAllByCity(@Param("kostLocation") String kostLocation, @Param("offset") Integer offset, @Param("limit") Integer limit);
    List<RumahKostMaster> findAllRumahDanKamar(@Param("offset") Integer offset, @Param("limit") Integer limit);



}
